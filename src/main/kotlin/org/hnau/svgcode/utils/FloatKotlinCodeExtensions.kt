package org.hnau.svgcode.utils

import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.ifEmpty


fun Float.toKotlinCode(
        maxFractionLength: Int?
) = maxFractionLength
        .checkNullable(
                ifNull = { toString() },
                ifNotNull = { maxFractionLengthNotNull ->
                    String.format("%.${maxFractionLengthNotNull}f", this)
                }
        )
        .split(".")
        .let { parts ->
            val main = parts[0]
            val fraction = parts.getOrNull(1)
            val fractionFixed = fraction?.dropLastWhile { it == '0' } ?: ""
            var suffix = "f"
            var separator = "."
            fractionFixed.ifEmpty {
                suffix = ""
                separator = ""
            }
            "$main$separator$fractionFixed$suffix"
        }