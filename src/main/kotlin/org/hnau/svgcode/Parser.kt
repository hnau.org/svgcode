package org.hnau.svgcode

import org.apache.batik.parser.PathParser
import org.hnau.svgcode.command.CommandsToAndroidPathBuilderCollector


object Parser {

    fun parse(
            svgPathData: String
    ) = CommandsToAndroidPathBuilderCollector()
            .also { commandsCollector ->
                PathParser().apply { pathHandler = commandsCollector }.parse(svgPathData)
            }
            .commands

}