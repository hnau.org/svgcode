package org.hnau.svgcode.point


data class Point(
        val x: Float,
        val y: Float
) {

    companion object {

        val zero = Point(0f, 0f)

    }

}