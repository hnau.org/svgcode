package org.hnau.svgcode.point


inline fun Point.combine(
        other: Point,
        combinator: (Float, Float) -> Float
) = Point(
        x = combinator(this.x, other.x),
        y = combinator(this.y, other.y)
)

operator fun Point.minus(other: Point) =
        combine(other, Float::minus)

operator fun Point.plus(other: Point) =
        combine(other, Float::plus)