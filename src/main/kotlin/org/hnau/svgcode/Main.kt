package org.hnau.svgcode

import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.HelpFormatter
import org.apache.commons.cli.Option
import org.apache.commons.cli.Options
import org.apache.commons.cli.ParseException
import org.hnau.svgcode.command.toKotlinCode
import kotlin.system.exitProcess


object Main {

    private const val maxFractionLengthOptionName = "maxFractionLength"
    private const val maxLineLengthOptionName = "maxLineLength"
    private const val addParamNamesOptionName = "addParamNames"
    private const val svgPathOptionName = "svgPath"

    @JvmStatic
    fun main(args: Array<String>) {

        val options = Options().apply {
            addOption(Option(
                    "f",
                    maxFractionLengthOptionName,
                    true,
                    "Max number fraction length"
            ))
            addOption(Option(
                    "l",
                    maxLineLengthOptionName,
                    true,
                    "Max line length"
            ))
            addOption(Option(
                    "p",
                    addParamNamesOptionName,
                    false,
                    "Add names to params"
            ))
            addOption(Option(
                    "d",
                    svgPathOptionName,
                    true,
                    "Svg path data"
            ))
        }

        val cmd = try {
            DefaultParser().parse(options, args)
        } catch (e: ParseException) {
            println(e.message)
            HelpFormatter().printHelp("SvgAndroid", options)
            exitProcess(1)
        }


        val svgPathData = cmd
                .getOptionValue(svgPathOptionName, null)
                ?: readLine()
                ?: throw IllegalArgumentException("Svg path data not presented")

        val commands = Parser.parse(svgPathData)
        val code = commands.toKotlinCode(
                maxFractionLength = cmd
                        .getOptionValue(maxFractionLengthOptionName, null)
                        ?.toInt(),
                maxLineLength = cmd
                        .getOptionValue(maxLineLengthOptionName, null)
                        ?.toInt(),
                addParamNames = cmd
                        .hasOption(addParamNamesOptionName)
        )

        println(code)

    }
}