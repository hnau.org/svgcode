package org.hnau.svgcode.command


enum class CommandType {

    moveTo,
    rMoveTo,
    lineTo,
    rLineTo,
    cubicTo,
    quadTo

}