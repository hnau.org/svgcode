package org.hnau.svgcode.command

import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.svgcode.point.Point
import org.hnau.svgcode.utils.toKotlinCode


@Suppress("ClassName")
sealed class Command(
        private val additionalParams: Iterable<Pair<Point, String>> = listOf(),
        val to: Point,
        private val kotlinCodeName: String
) {

    class line(
            to: Point
    ) : Command(
            to = to,
            kotlinCodeName = "l"
    )

    class move(
            to: Point
    ) : Command(
            to = to,
            kotlinCodeName = "m"
    )

    class quadratic(
            val controlPoint: Point,
            to: Point
    ) : Command(
            additionalParams = listOf(controlPoint to "1"),
            to = to,
            kotlinCodeName = "q"
    )

    class cubic(
            val controlPoint1: Point,
            val controlPoint2: Point,
            to: Point
    ) : Command(
            additionalParams = listOf(
                    controlPoint1 to "1",
                    controlPoint2 to "2"
            ),
            to = to,
            kotlinCodeName = "c"
    )

    fun toKotlinCode(
            maxFractionLength: Int?,
            addParamNames: Boolean
    ) =
            (additionalParams + (to to ""))
                    .map { (point, name) ->
                        listOf(
                                point.x to "x$name",
                                point.y to "y$name"
                        )
                    }
                    .flatten()
                    .joinToString(
                            separator = ", ",
                            transform = { (value, name) ->
                                val prefix = addParamNames.checkBoolean(
                                        ifTrue = { "$name = " },
                                        ifFalse = { "" }
                                )
                                prefix + value.toKotlinCode(maxFractionLength)
                            }
                    )
                    .let { params ->
                        "$kotlinCodeName($params)"
                    }

}