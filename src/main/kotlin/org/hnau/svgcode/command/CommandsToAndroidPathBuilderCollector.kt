package org.hnau.svgcode.command

import org.apache.batik.parser.PathHandler
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.castOrNull
import org.hnau.svgcode.point.Point


class CommandsToAndroidPathBuilderCollector : PathHandler {

    private var initialPoint = Point.zero

    private val commandsInner = ArrayList<Command>()
    val commands: List<Command> get() = commandsInner

    private val lastCommand
        get() = commands.lastOrNull()

    private val currentPoint: Point
        get() = lastCommand?.to ?: Point.zero

    private fun command(
            command: Command,
            resetInitialPoint: Boolean = false
    ) {
        commandsInner.add(command)
        resetInitialPoint.ifTrue { initialPoint = currentPoint }
    }

    override fun closePath() =
            command(Command.line(initialPoint))

    override fun curvetoCubicRel(
            x1: Float,
            y1: Float,
            x2: Float,
            y2: Float,
            x: Float,
            y: Float
    ) = curvetoCubicAbs(
            x1 = currentPoint.x + x1,
            y1 = currentPoint.y + y1,
            x2 = currentPoint.x + x2,
            y2 = currentPoint.y + y2,
            x = currentPoint.x + x,
            y = currentPoint.y + x
    )

    override fun movetoAbs(
            x: Float,
            y: Float
    ) = command(
            command = Command.move(Point(x, y)),
            resetInitialPoint = true
    )

    override fun startPath() {}

    override fun arcRel(
            rx: Float,
            ry: Float,
            xAxisRotation: Float,
            largeArcFlag: Boolean,
            sweepFlag: Boolean,
            x: Float,
            y: Float
    ) = arcAbs(
            rx = rx,
            ry = ry,
            xAxisRotation = xAxisRotation,
            largeArcFlag = largeArcFlag,
            sweepFlag = sweepFlag,
            x = currentPoint.x + x,
            y = currentPoint.y + y
    )

    override fun curvetoCubicSmoothAbs(
            x2: Float,
            y2: Float,
            x: Float,
            y: Float
    ) {
        var x1 = x2
        var y1 = y2
        lastCommand
                ?.castOrNull<Command.cubic>()
                ?.let { lastCommand ->
                    x1 = currentPoint.x - lastCommand.controlPoint2.x
                    y1 = currentPoint.y - lastCommand.controlPoint2.y
                }
        curvetoCubicAbs(
                x1 = x1,
                y1 = y1,
                x2 = x2,
                y2 = y2,
                x = x,
                y = y
        )
    }


    override fun curvetoQuadraticRel(
            x1: Float,
            y1: Float,
            x: Float,
            y: Float
    ) = curvetoQuadraticAbs(
            x1 = currentPoint.x + x1,
            y1 = currentPoint.y + y1,
            x = currentPoint.x + x,
            y = currentPoint.y + y
    )

    override fun linetoHorizontalAbs(x: Float) =
            linetoAbs(x, currentPoint.y)

    override fun curvetoQuadraticAbs(
            x1: Float,
            y1: Float,
            x: Float,
            y: Float
    ) = command(
            Command.quadratic(
                    controlPoint = Point(x1, y1),
                    to = Point(x, y)
            )
    )

    override fun linetoHorizontalRel(x: Float) =
            linetoRel(x, 0f)

    override fun linetoVerticalRel(y: Float) =
            linetoRel(0f, y)

    override fun linetoAbs(x: Float, y: Float) =
            command(Command.line(Point(x, y)))

    override fun linetoVerticalAbs(y: Float) =
            linetoAbs(currentPoint.x, y)

    override fun curvetoCubicSmoothRel(
            x2: Float,
            y2: Float,
            x: Float,
            y: Float
    ) = curvetoCubicSmoothAbs(
            x2 = currentPoint.x + x2,
            y2 = currentPoint.y + y2,
            x = currentPoint.x + x,
            y = currentPoint.y + y
    )

    override fun linetoRel(
            x: Float,
            y: Float
    ) = linetoAbs(
            x = currentPoint.x + x,
            y = currentPoint.y + y
    )

    override fun curvetoCubicAbs(
            x1: Float,
            y1: Float,
            x2: Float,
            y2: Float,
            x: Float,
            y: Float
    ) = command(
            Command.cubic(
                    controlPoint1 = Point(x1, y1),
                    controlPoint2 = Point(x2, y2),
                    to = Point(x, y)
            )
    )

    override fun movetoRel(
            x: Float,
            y: Float
    ) = movetoAbs(
            x = currentPoint.x + x,
            y = currentPoint.y + y
    )

    override fun arcAbs(
            rx: Float,
            ry: Float,
            xAxisRotation: Float,
            largeArcFlag: Boolean,
            sweepFlag: Boolean,
            x: Float,
            y: Float
    ) = PathSvgArcDrawer.arcTo(
            currentPoint.x, currentPoint.y,
            rx, ry,
            xAxisRotation, largeArcFlag, sweepFlag,
            x, y
    ) { command ->
        command(command)
    }

    override fun curvetoQuadraticSmoothAbs(
            x: Float,
            y: Float
    ) {
        var x1 = currentPoint.x
        var y1 = currentPoint.y
        lastCommand
                ?.castOrNull<Command.quadratic>()
                ?.let { lastCommand ->
                    x1 = currentPoint.x - lastCommand.controlPoint.x
                    y1 = currentPoint.y - lastCommand.controlPoint.y
                }
        curvetoQuadraticAbs(
                x1 = x1,
                y1 = y1,
                x = x,
                y = y
        )
    }

    override fun endPath() {}

    override fun curvetoQuadraticSmoothRel(
            x: Float,
            y: Float
    ) = curvetoQuadraticSmoothAbs(
            x = currentPoint.x + x,
            y = currentPoint.y + y
    )

}