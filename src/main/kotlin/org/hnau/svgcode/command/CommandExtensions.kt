package org.hnau.svgcode.command

import org.hnau.base.extensions.number.ifPositive
import org.hnau.base.extensions.number.takeIfPositive


private const val commandsSeparator = "; "
private const val commandsSeparatorLength = commandsSeparator.length

@Suppress("LiftReturnOrAssignment")
fun Iterable<Command>.toKotlinCode(
        maxFractionLength: Int?,
        maxLineLength: Int?,
        addParamNames: Boolean
): String {
    val result = StringBuilder()
    var currentLineLength = 0
    forEach { command ->
        val commandCode = command.toKotlinCode(maxFractionLength, addParamNames)
        val commandLength = commandCode.length
        if (maxLineLength != null && commandLength > maxLineLength) {
            throw RuntimeException("Length of command '$commandCode' is large than maxLineLength ($maxLineLength)")
        }
        val currentLineLengthLocal = currentLineLength.takeIfPositive()
                ?.plus(commandsSeparatorLength)
                ?: 0
        if (maxLineLength != null && currentLineLengthLocal + commandLength > maxLineLength) {
            result.append("\n")
            currentLineLength = 0
        } else {
            currentLineLength.ifPositive { result.append(commandsSeparator) }
            currentLineLength = currentLineLengthLocal
        }
        result.append(commandCode)
        currentLineLength += commandLength
    }
    return result.toString()
}