package org.hnau.svgcode

import org.assertj.core.api.Assertions.assertThat
import org.hnau.svgcode.utils.toKotlinCode
import org.junit.Test


class FloatToKotlinCodeTest {

    @Test
    fun test() {

        listOf(
                Triple(0f, "0", 3),
                Triple(0f, "0", 0),
                Triple(0.0f, "0", 3),
                Triple(0.12345f, "0.123f", 3),
                Triple(123f, "123", 3),
                Triple(1.0e+4f, "10000", 3),
                Triple(1.0e-4f, "0", 3),
                Triple(0.135790f, "0.13579f", null)
        ).forEach { (float, expectedString, maxFractionLength) ->
            val actualString = float.toKotlinCode(maxFractionLength)
            assertThat(actualString).isEqualTo(expectedString)
        }

    }

}